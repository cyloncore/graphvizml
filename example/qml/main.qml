import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Window 2.0
import QtQuick.Layouts 1.0

import GraphvizML 1.0
import GraphvizML.Model 1.0

Window
{
  visible: true
  width: 800
  height: 600
  
  GraphView
  {
    anchors.fill: parent
    nodeDelegate: Rectangle {
      color: "lightblue"
      width: 150
      height: 50
      Text
      {
        anchors.centerIn: parent
        text: node
      }
    }
    model: Graph
    {
      Node
      {
        id: n1
        data: "Hello"
      }
      Node
      {
        id: n2
        data: "World"
      }
      Node
      {
        id: n3
        data: "Universe"
      }
      Node
      {
        id: n4
        data: "Black hole"
      }
      Edge
      {
        source: n1
        destination: n2
      }
      Edge
      {
        source: n1
        destination: n3
      }
      Edge
      {
        source: n2
        destination: n4
      }
      Edge
      {
        source: n3
        destination: n4
      }
    }
  }
}

