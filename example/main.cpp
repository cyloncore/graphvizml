#include <QGuiApplication>
#include <QQmlApplicationEngine>

const char* applicationName = "GraphvizML_example";

int main(int argc, char *argv[])
{
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  QGuiApplication app(argc, argv);
  QQmlApplicationEngine engine;
  engine.addImportPath("qrc:/qml");
  engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
  return app.exec();
}
