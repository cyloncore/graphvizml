#pragma once

#include <QQuickItem>

class QQmlComponent;

namespace GraphvizML
{
  namespace Model
  {
    class Graph;
  }
  class GraphViewItem : public QQuickItem
  {
    Q_OBJECT
    Q_PROPERTY(GraphvizML::Model::Graph* model READ model WRITE setModel NOTIFY modelChanged)
    Q_PROPERTY(QQmlComponent* nodeDelegate READ nodeDelegate WRITE setNodeDelegate NOTIFY nodeDelegateChanged)
    Q_PROPERTY(QQmlComponent* edgeDelegate READ edgeDelegate WRITE setEdgeDelegate NOTIFY edgeDelegateChanged)
  public:
    GraphViewItem(QQuickItem* _parent = nullptr);
    ~GraphViewItem();
  public:
    Model::Graph* model() const;
    void setModel(Model::Graph* _model);
    QQmlComponent* nodeDelegate() const;
    void setNodeDelegate(QQmlComponent* _nodeDelegate);
    QQmlComponent* edgeDelegate() const;
    void setEdgeDelegate(QQmlComponent* _edgeDelegate);
  signals:
    void modelChanged();
    void nodeDelegateChanged();
    void edgeDelegateChanged();
  private:
    struct Private;
    Private* const d;
  };
}
