#include "Node.h"

#include <QVariant>

using namespace GraphvizML::Model;

struct Node::Private
{
  QVariant data;
};

Node::Node(QObject* _parent) : QObject(_parent), d(new Private)
{
}

Node::~Node()
{
}

QVariant Node::data() const
{
  return d->data;
}

void Node::setData(const QVariant& _data)
{
  d->data = _data;
  emit(dataChanged());
  emit(updated());
}

#include "moc_Node.cpp"
