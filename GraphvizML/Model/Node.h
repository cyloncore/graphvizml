#pragma once

#include <QObject>

namespace GraphvizML::Model
{
  class Node : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(QVariant data READ data WRITE setData NOTIFY dataChanged)
  public:
    Node(QObject* _parent = nullptr);
    ~Node();
    QVariant data() const;
    void setData(const QVariant& _data);
  signals:
    void dataChanged();
    void updated();
  private:
    struct Private;
    Private* const d;
  };
}

