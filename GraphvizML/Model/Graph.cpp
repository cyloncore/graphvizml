#include "Graph.h"

#include <QDebug>
#include <QFile>
#include <QJSEngine>
#include <QJSValueList>
#include <QUrl>

#include "Edge.h"
#include "Node.h"

using namespace GraphvizML::Model;

struct Graph::Private
{
  
  QList<Node*> nodes;
  QList<Edge*> edges;
  QList<QObject*> elements;
  
  static void appendElement(QQmlListProperty<QObject>*, QObject*);
  static int elementCount(QQmlListProperty<QObject>*);
  static QObject* element(QQmlListProperty<QObject>*, int);
  static void clearElements(QQmlListProperty<QObject>*);
};

void Graph::Private::appendElement(QQmlListProperty<QObject>* _property, QObject* _object)
{
  Graph* graph = qobject_cast<Graph*>(_property->object);
  Node* n = qobject_cast<Node*>(_object);
  if(n)
  {
    graph->addNode(n);
  } else {
    Edge* e = qobject_cast<Edge*>(_object);
    if(e)
    {
      graph->addEdge(e);
    }
    else
    {
      qWarning().nospace() << "Invalid object '" << _object << "' excpected Node or Edge";
      Private* self = reinterpret_cast<Private*>(_property->data);
      self->elements.append(_object);
      emit(graph->updated());
    }
  }
}

int Graph::Private::elementCount(QQmlListProperty<QObject>* _property)
{
  return reinterpret_cast<Private*>(_property->data)->elements.count();
}

QObject * Graph::Private::element(QQmlListProperty<QObject>* _property, int _index)
{
  return reinterpret_cast<Private*>(_property->data)->elements.at(_index);
}

void Graph::Private::clearElements(QQmlListProperty<QObject>* _property)
{
  Private* self = reinterpret_cast<Private*>(_property->data);
  self->elements.clear();
  self->nodes.clear();
  self->edges.clear();
  emit(qobject_cast<Graph*>(_property->object)->updated());
}


Graph::Graph(QObject* _parent) : QObject(_parent), d(new Private)
{
}

Graph::~Graph()
{
}

QList<Edge *> Graph::edges() const
{
  return d->edges;
}

void Graph::addEdge(Edge* _edge)
{
  d->edges.append(_edge);
  d->elements.append(_edge);
  QObject::connect(_edge, SIGNAL(updated()), SIGNAL(updated()));
  emit(updated());
}

Edge* Graph::createEdge(Node* _source, Node* _destination)
{
  Edge* edge = new Edge;
  edge->setSource(_source);
  edge->setDestination(_destination);
  addEdge(edge);
  return edge;
}

QList<Node *> Graph::nodes() const
{
  return d->nodes;
}

void Graph::addNode(Node* _node)
{
  d->nodes.append(_node);
  d->elements.append(_node);
  QObject::connect(_node, SIGNAL(updated()), SIGNAL(updated()));
  emit(updated());
}


QQmlListProperty<QObject> Graph::elements()
{
    return QQmlListProperty<QObject>(this, d,
             &Private::appendElement,
             &Private::elementCount,
             &Private::element,
             &Private::clearElements);
}

bool Graph::exportToDot(const QUrl& _destination, const QVariant& _labelMaker)
{
  if(_destination.isLocalFile())
  {
    QFile file(_destination.toLocalFile());
    if(file.open(QIODevice::WriteOnly))
    {
      file.write(toDot(_labelMaker).toUtf8());
      return true;
    } else {
      qWarning() << "Failed to open " << _destination << " for writting";
      return false;
    }
  } else {
    qWarning() << _destination << " is not a local file!";
    return false;
  }
}

QString Graph::toDot(const QVariant& _labelMaker)
{
  QString result;
  
  QHash<Node*, QString> node2label;
  
  for(Node* node : d->nodes)
  {
    QString label = QString("n%1").arg(node2label.size());
    node2label[node] = label;
    if(_labelMaker.canConvert<QJSValue>())
    {
      QJSValue value = _labelMaker.value<QJSValue>();
      QJSValue argument;
      QVariant node_data = node->data();
      if(node_data.canConvert<QObject*>())
      {
        argument = qjsEngine(this)->toScriptValue(node_data.value<QObject*>());
      } else {
        argument = qjsEngine(this)->toScriptValue(node_data);
      }
      
      QString res = value.call(QJSValueList() << argument).toString();
      if(not res.isEmpty())
      {
        result += QString("  %1 [label=\"%2\"];\n").arg(label).arg(res);
      }
    }
  }
  for(Edge* edge : d->edges)
  {
    if(node2label.contains(edge->source()) and node2label.contains(edge->destination()))
    {
      result += QString("  %1 -> %2;\n").arg(node2label.value(edge->source())).arg(node2label.value(edge->destination()));
    }
  }
  
  return "digraph graphname {\n" + result + "}\n";
}


#include "moc_Graph.cpp"
