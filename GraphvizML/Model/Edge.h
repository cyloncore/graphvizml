#pragma once

#include <QObject>

namespace GraphvizML::Model
{
  class Node;
  class Edge : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(GraphvizML::Model::Node* source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(GraphvizML::Model::Node* destination READ destination WRITE setDestination NOTIFY destinationChanged)
  public:
    Edge(QObject* _parent = nullptr);
    ~Edge();
    /**
     * @return true if the edge is connected, ie, if the source and destination are set
     */
    bool isConnected() const;
    Node* source() const;
    void setSource(Node* _source);
    Node* destination() const;
    void setDestination(Node* _destination);
  signals:
    void sourceChanged();
    void destinationChanged();
    void updated();
  private:
    struct Private;
    Private* const d;
  };
}
