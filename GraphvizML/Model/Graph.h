#pragma once

#include <QQmlListProperty>

#include <QObject>

namespace GraphvizML::Model
{
  class Edge;
  class Node;
  class Graph : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<QObject> elements READ elements NOTIFY updated)
    Q_CLASSINFO("DefaultProperty", "elements")
  public:
    Graph(QObject* _parent = nullptr);
    ~Graph();
    void addNode(Node* _node);
    void addEdge(Edge* _edge);
    QList<Edge*> edges() const;
    QList<Node*> nodes() const;
    Edge* createEdge(Node* _source, Node* _destination);
    QQmlListProperty<QObject> elements();
    Q_INVOKABLE bool exportToDot(const QUrl& _destination, const QVariant& _labelMaker);
    Q_INVOKABLE QString toDot(const QVariant& _labelMaker);
  signals:
    void updated();
  private:
    struct Private;
    Private* const d;
  };
}


