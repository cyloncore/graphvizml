#include "Edge.h"

using namespace GraphvizML::Model;

struct Edge::Private
{
  Node* source = nullptr;
  Node* destination = nullptr;
};

Edge::Edge(QObject* _parent) : QObject(_parent), d(new Private)
{
}

Edge::~Edge()
{
}

bool Edge::isConnected() const
{
  return d->source and d->destination;
}

Node * Edge::destination() const
{
  return d->destination;
}

void Edge::setDestination(Node* _destination)
{
  d->destination = _destination;
  emit(updated());
  emit(destinationChanged());
}

Node * Edge::source() const
{
  return d->source;
}

void Edge::setSource(Node* _source)
{
  d->source = _source;
  emit(updated());
  emit(sourceChanged());
}

#include "moc_Edge.cpp"
