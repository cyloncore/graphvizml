#include "GraphViewItem.h"

#include <graphviz/cgraph.h>
#include <graphviz/gvc.h>

#include <QQmlEngine>
#include <QQmlContext>
#include <QPainterPath>

#include "Model/Edge.h"
#include "Model/Graph.h"
#include "Model/Node.h"

#include "DefaultEdgeDelegate.h"

using namespace GraphvizML;

struct GraphViewItem::Private
{
  Model::Graph* model = nullptr;
  QMetaObject::Connection modelUpdatedCI;
  QQmlComponent* edgeDelegate = nullptr;
  QQmlComponent* nodeDelegate = nullptr;
    
  struct NodeInfo
  {
    QQuickItem* item = nullptr;
    Agnode_t* node = nullptr;
    void clean();
  };
  struct EdgeInfo
  {
    QQuickItem* item = nullptr;
    Agedge_t* edge = nullptr;
    void clean();
  };
  QHash<Model::Node*, NodeInfo> nodeInfos;
  QHash<Model::Edge*, EdgeInfo> edgeInfos;
  
  template<typename _TAttr_, typename _T_>
  static void setAttribute(_T_* _t, const char* _name, const _TAttr_& _value, const _TAttr_& _defaultValue = _TAttr_());

  template<typename _TAttr_, typename _T_>
  static _TAttr_ getAttribute(_T_* _t, const char* _name, const _TAttr_& _defaultValue = _TAttr_());
  
  static constexpr qreal DotDefaultDPI = 72.0;

  template<typename _T_>
  QPointF toPoint(_T_ _t)
  {
    return QPointF(_t.x, GD_bb(gv_graph).UR.y - _t.y);
  }
  QPointF getCoord(Agnode_t* _t)
  {
    return toPoint(ND_coord(_t));
  }
  
  QPainterPath getSpline(Agedge_t* _e);
  
  GraphViewItem* self;
  
  Agraph_t* gv_graph = nullptr;
  GVC_t* gv_context = nullptr;

  
  void clear();
  void update();
};

template<typename _TAttr_, typename _T_>
void GraphViewItem::Private::setAttribute(_T_* _t, const char* _name, const _TAttr_& _value, const _TAttr_& _defaultValue)
{
  agsafeset(_t, const_cast<char*>(_name), const_cast<char*>(qPrintable(QVariant::fromValue(_value).toString())),  const_cast<char*>(qPrintable(QVariant::fromValue(_defaultValue).toString())));
}

template<typename _TAttr_, typename _T_>
_TAttr_ GraphViewItem::Private::getAttribute(_T_* _t, const char* _name, const _TAttr_& _defaultValue)
{
  const char* v = agget(_t, _name);
  if(v)
  {
    return QVariant(v).value<_T_>();
  } else {
    return _defaultValue;
  }
}

QPainterPath GraphvizML::GraphViewItem::Private::getSpline(Agedge_t* _e)
{
  const splines* spl = ED_spl(_e);
  QPainterPath path;
  if((spl->list != 0) && (spl->list->size%3 == 1))
  {
    bezier bez = spl->list[0];
    //If there is a starting point, draw a line from it to the first curve point
    if(bez.sflag)
    {
      path.moveTo(toPoint(bez.sp));
      path.lineTo(toPoint(bez.list[0]));
    }
    else
    {
      path.moveTo(toPoint(bez.list[0]));
    }

    //Loop over the curve points
    for(int i=1; i<bez.size; i+=3)
    {
      path.cubicTo(toPoint(bez.list[i]), toPoint(bez.list[i+1]), toPoint(bez.list[i+2]));
    }

    //If there is an ending point, draw a line to it
    if(bez.eflag)
    {
      path.lineTo(toPoint(bez.ep));
    }
  }
  return path;

}

void GraphViewItem::Private::NodeInfo::clean()
{
  if(item)
  {
    item->deleteLater();
    item = nullptr;
  }
}

void GraphViewItem::Private::EdgeInfo::clean()
{
  if(item)
  {
    item->deleteLater();
    item = nullptr;
  }
}

void GraphViewItem::Private::clear()
{
  for(auto it = nodeInfos.begin(); it != nodeInfos.end(); ++it)
  {
    it.value().clean();
  }
  nodeInfos.clear();
  for(auto it = edgeInfos.begin(); it != edgeInfos.end(); ++it)
  {
    it.value().clean();
  }
  edgeInfos.clear();
  
  if(gv_graph)
  {
    agclose(gv_graph);
    gv_graph = nullptr;
  }
}

void GraphViewItem::Private::update()
{
  if(model)
  {
    if(not gv_graph)
    {
      Q_ASSERT(nodeInfos.isEmpty());
      Q_ASSERT(edgeInfos.isEmpty());
      gv_graph = agopen(const_cast<char*>("GraphvizML Graph"), Agdirected, 0);
    }
    // 1) update node infos
    QList<Model::Node*> nodes = model->nodes();
    for(Model::Node*  node : nodes)
    {
      Q_ASSERT(node);
      if(not nodeInfos.contains(node))
      {
        NodeInfo ni;
        if(nodeDelegate)
        {
          QQmlContext* context = new QQmlContext(QQmlEngine::contextForObject(self), self);
          context->setContextProperty("node", node->data());
          ni.item = qobject_cast<QQuickItem*>(nodeDelegate->create(context));
          ni.item->setParentItem(self);
        }
        ni.node = agnode(gv_graph, nullptr, 1);
        Q_ASSERT(ni.node);
        nodeInfos[node] = ni;
      }
    }
    for(Model::Node* node : nodeInfos.keys())
    {
      if(nodes.contains(node))
      {
        NodeInfo& ni = nodeInfos[node];
        if(ni.item)
        {
          setAttribute(ni.node, "width", ni.item->width() / DotDefaultDPI);
          setAttribute(ni.node, "height", ni.item->height() / DotDefaultDPI);
        }
      }
      else
      {
        nodeInfos[node].clean();
        agdelete(gv_graph, nodeInfos[node].node);
        nodeInfos.remove(node);
      }
    }
    Q_ASSERT(nodes.size() == nodeInfos.size());
    // 2) update edge info
    QList<Model::Edge*> edges = model->edges();
    for(Model::Edge*  edge : edges)
    {
      if(not edgeInfos.contains(edge))
      {
        EdgeInfo ni;
        if(edgeDelegate)
        {
          ni.item = qobject_cast<QQuickItem*>(edgeDelegate->create(nullptr));
          ni.item->setParentItem(self);
        } else {
          ni.item = new DefaultEdgeDelegate(self);
        }
        edgeInfos[edge] = ni;
      }
    }
    for(Model::Edge* edge : edgeInfos.keys())
    {
      EdgeInfo& ei = edgeInfos[edge];
      if(ei.edge)
      {
        agdelete(gv_graph, ei.edge);
      }
      if(edges.contains(edge))
      { // Always redo the edges
        if(nodeInfos.contains(edge->source()) and nodeInfos.contains(edge->destination()))
        {
          ei.edge = agedge(gv_graph, nodeInfos[edge->source()].node, nodeInfos[edge->destination()].node, nullptr, 1);
        }
      }
      else
      {
        ei.clean();
        edgeInfos.remove(edge);
      }
    }
    Q_ASSERT(edges.size() == edgeInfos.size());
    // 3) update layout
    if(gvLayout(gv_context, gv_graph, "dot") != 0)
    {
      qWarning() << "Failed to layout graph!";
      clear();
      return;
    }
    // 4) apply layout on nodes
    for(auto it = nodeInfos.begin(); it != nodeInfos.end(); ++it)
    {
      if(it->item)
      {
        it->item->setPosition(getCoord(it->node) - 0.5 * QPointF(it->item->width(), it->item->height()));
      }
    }

    // 5) apply layout on edges
    for(auto it = edgeInfos.begin(); it != edgeInfos.end(); ++it)
    {
      if(it->item and it->edge)
      {
        QPainterPath path = getSpline(it->edge);
        QRectF br = path.boundingRect();
        it->item->setPosition(br.topLeft() - QPointF(1, 1));
        it->item->setSize(br.size() + QSize(2, 2));
        path.translate(-br.topLeft()+QPointF(1, 1));
        it->item->setProperty("path", QVariant::fromValue(path));
      }
    }
    
    // 6) apply bounding box
    self->setWidth(GD_bb(gv_graph).UR.x);
    self->setHeight(GD_bb(gv_graph).UR.y);
  }
}


GraphViewItem::GraphViewItem(QQuickItem* _parent) : QQuickItem(_parent), d(new Private)
{
  d->self = this;
  d->gv_context = gvContext();
}

GraphViewItem::~GraphViewItem()
{
  d->clear();
  gvFreeContext(d->gv_context);
  delete d;
}

Model::Graph* GraphViewItem::model() const
{
  return d->model;
}

void GraphViewItem::setModel(Model::Graph* _model)
{
  QObject::disconnect(d->modelUpdatedCI);
  d->model = _model;
  emit(modelChanged());
  d->modelUpdatedCI = QObject::connect(d->model, &Model::Graph::updated, [this] {
    d->update();
  });
  d->clear();
  d->update();
}

QQmlComponent * GraphViewItem::edgeDelegate() const
{
  return d->edgeDelegate;
}

void GraphViewItem::setEdgeDelegate(QQmlComponent* _edgeDelegate)
{
  d->edgeDelegate = _edgeDelegate;
  emit(edgeDelegateChanged());
  d->clear();
  d->update();
}

QQmlComponent * GraphViewItem::nodeDelegate() const
{
  return d->nodeDelegate;
}

void GraphViewItem::setNodeDelegate(QQmlComponent* _nodeDelegate)
{
  d->nodeDelegate = _nodeDelegate;
  emit(nodeDelegateChanged());
  d->clear();
  d->update();
}

#include "moc_GraphViewItem.cpp"
