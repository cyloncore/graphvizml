#pragma once

#include <QQuickPaintedItem>

class QPainterPath;

namespace GraphvizML
{
  // TODO update with new Shapes stuff in Qt 5.10
  class DefaultEdgeDelegate : public QQuickPaintedItem
  {
    Q_OBJECT
    Q_PROPERTY(QPainterPath path READ path WRITE setPath NOTIFY pathChanged)
  public:
    DefaultEdgeDelegate(QQuickItem* _parent);
    ~DefaultEdgeDelegate();
    QPainterPath path() const;
    void setPath(const QPainterPath& _path);
  protected:
    void paint(QPainter *painter) override;
  signals:
    void pathChanged();
  private:
    struct Private;
    Private* const d;
  };
}

Q_DECLARE_METATYPE (QPainterPath)
