#include "DefaultEdgeDelegate.h"

#include <QPainterPath>
#include <QPainter>

using namespace GraphvizML;

struct DefaultEdgeDelegate::Private
{
  QPainterPath path;
};

DefaultEdgeDelegate::DefaultEdgeDelegate(QQuickItem* _parent) : QQuickPaintedItem(_parent), d(new Private)
{
}

DefaultEdgeDelegate::~DefaultEdgeDelegate()
{
  delete d;
}

QPainterPath DefaultEdgeDelegate::path() const
{
  return d->path;
}

void DefaultEdgeDelegate::setPath(const QPainterPath& _path)
{
  d->path = _path;
  emit(pathChanged());
  update();
}

void DefaultEdgeDelegate::paint(QPainter* _painter)
{
  _painter->drawPath(d->path);
}

#include "moc_DefaultEdgeDelegate.cpp"
