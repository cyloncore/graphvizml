#pragma once

#include <QQmlExtensionPlugin>

class GraphvizMLPlugin: public QQmlExtensionPlugin {
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "GraphvizML/1.0")
public:
  
  void registerTypes(const char* uri) override;
};
