#include "GraphvizMLPlugin.h"

#include <GraphvizML/GraphViewItem.h>

#include <GraphvizML/Model/Edge.h>
#include <GraphvizML/Model/Graph.h>
#include <GraphvizML/Model/Node.h>

using namespace GraphvizML;

void GraphvizMLPlugin::registerTypes(const char* /*uri*/)
{
  const char* uri_GraphvizML = "GraphvizML";
  qmlRegisterType<GraphViewItem>(uri_GraphvizML, 1, 0, "GraphView");

  const char* uri_GraphvizML_Model = "GraphvizML.Model";
  qmlRegisterType<Model::Edge >(uri_GraphvizML_Model, 1, 0, "Edge");
  qmlRegisterType<Model::Graph>(uri_GraphvizML_Model, 1, 0, "Graph");
  qmlRegisterType<Model::Node >(uri_GraphvizML_Model, 1, 0, "Node");
}

#include "moc_GraphvizMLPlugin.cpp"
